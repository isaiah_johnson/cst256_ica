<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Business\SecurityService;
use App\Models\UserModel;

class LoginController extends Controller
{
    public function authenticate(Request $request){
        $username = $request->input("username");
        $password = $request->input("password");
        
        $model = new UserModel($username, $password);
        
        $service = new SecurityService();
        
        $result = $service->authenticate($model);
        
        if($result){
            return view("loginPassed2")->with('model', $model);
        }
        else{
            return view("loginFailed");
        }
    }
}
