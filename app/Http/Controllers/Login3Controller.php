<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Business\SecurityService;
use App\Models\UserModel;

class Login3Controller extends Controller
{
    public function index(Request $request){
        $username = $request->input("username");
        $password = $request->input("password");
        
        
        $this->validateForm($request);
        
        $model = new UserModel($username, $password);
        
        $service = new SecurityService();
        
        $result = $service->authenticate($model);
        
        if($result){
            return view("loginPassed2")->with('model', $model);
        }
        else{
            return view("loginFailed");
        }
    }
    
    private function validateForm(Request $request)
    {
        // Setup Data Validation Rules for Login Form
        $rules = ['username' => 'Required | Between:4,20',
            'password' => 'Required | Between:4,15'];
        
        // Run Data Validation Rules
        $this->validate($request, $rules);
    }
    
}
