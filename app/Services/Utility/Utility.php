<?php
namespace App\Services\Utility;

use Exception;
use \PDO;

class Utility
{
    
    public function getConn() {
        try {
            $hostname = config("database.connections.mysql.host");
            $username = config("database.connections.mysql.username");
            $password = config("database.connections.mysql.password");
            $dbname = config("database.connections.mysql.database");
            
            $conn = new PDO("mysql:host=$hostname;dbname=$dbname",$username,$password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
        } catch (Exception $e) {
            throw $e;
        }
        
        return $conn;
    }
}