<?php
namespace App\Services\Business;

use App\Services\Data\SecurityDAO;
use App\Services\Utility\Utility;

class SecurityService
{
    public function authenticate($user)
    {
        //Get PDO connection
        $util = new Utility();
        $conn = $util->getConn();
        
        $securityDAO = new SecurityDAO($conn);
        $db_user = $securityDAO->findByUsername($user);
        
        if($db_user["PASSWORD"] == $user->getPassword())
        {
            return true;
        }
        
        return false;
    }
}

