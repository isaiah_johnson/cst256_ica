<?php
namespace App\Services\Data;

use PDO;
use PDOException;

class SecurityDAO
{

    private $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }
    
    public function findByUsername($user)
    {        
        $username = $user->getUsername();
        try {
          //Create sql query
          $sql_query = $this->conn->prepare("SELECT * FROM USER WHERE USERNAME = :username");
          //Bind data
          $sql_query->bindParam(':username', $username);
          //Execute query
          $sql_query->execute();
          //Get rows returned from query
          $result = $sql_query->fetch(PDO::FETCH_ASSOC);
          //Return the result
          return $result;
      }
      //Catch PDO exception
      catch (PDOException $e) {
          //TODO: throw custom exception
          throw $e;
      }
    }
}

