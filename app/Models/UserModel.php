<?php
namespace App\Models;

class UserModel
{
    private $username;
    private $password;
    
    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }
    
    
}

